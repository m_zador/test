<?php

include("./ADOdb-5.20.18/adodb.inc.php");
$db = newADOConnection('mysqli');
$db->connect("localhost", "root", "", "Books");

$tr = "SELECT books.name, COUNT(books_authors.id) as autors_count
FROM books 
LEFT JOIN books_authors ON books.id = books_authors.book_id
GROUP BY books.id
HAVING autors_count > 2";

$db->setFetchMode(ADODB_FETCH_ASSOC);
$result = $db->execute($tr);
if ($result === false) die("failed");

print_r($result->getRows());

